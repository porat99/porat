all: hello clean 

hello: helper.o main_porat.o 
	gcc main_porat.o helper.o -o hello3
main_porat.o: main_porat.c
	gcc -c main_porat.c
helper.o: helper.c
	gcc -c helper.c
clean:
	rm -rf *o hello

run: 
	gdb hello3
delt:
	rm -rf hello3
